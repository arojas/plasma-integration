# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Alexander Potashev <aspotashev@gmail.com>, 2014, 2016.
# Alexander Yavorsky <kekcuha@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-22 01:40+0000\n"
"PO-Revision-Date: 2023-01-12 11:23+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.3\n"

#: platformtheme/kdeplatformfiledialoghelper.cpp:274
#, kde-format
msgctxt "@title:window"
msgid "Open File"
msgstr "Открытие файла"

#: platformtheme/kdeplatformfiledialoghelper.cpp:275
#, kde-format
msgctxt "@title:window"
msgid "Save File"
msgstr "Сохранение файла"

#: platformtheme/kdeplatformtheme.cpp:402
#, kde-format
msgctxt "@action:button"
msgid "Save All"
msgstr "Сохранить все"

#: platformtheme/kdeplatformtheme.cpp:406
#, kde-format
msgctxt "@action:button"
msgid "&Yes"
msgstr "&Да"

#: platformtheme/kdeplatformtheme.cpp:408
#, kde-format
msgctxt "@action:button"
msgid "Yes to All"
msgstr "Да для всех"

#: platformtheme/kdeplatformtheme.cpp:410
#, kde-format
msgctxt "@action:button"
msgid "&No"
msgstr "&Нет"

#: platformtheme/kdeplatformtheme.cpp:412
#, kde-format
msgctxt "@action:button"
msgid "No to All"
msgstr "Нет для всех"

#: platformtheme/kdeplatformtheme.cpp:415
#, kde-format
msgctxt "@action:button"
msgid "Abort"
msgstr "Прервать"

#: platformtheme/kdeplatformtheme.cpp:417
#, kde-format
msgctxt "@action:button"
msgid "Retry"
msgstr "Повторить"

#: platformtheme/kdeplatformtheme.cpp:419
#, kde-format
msgctxt "@action:button"
msgid "Ignore"
msgstr "Пропустить"

#: platformtheme/kdirselectdialog.cpp:122
#, kde-format
msgctxt "folder name"
msgid "New Folder"
msgstr "Новая папка"

#: platformtheme/kdirselectdialog.cpp:128
#, kde-format
msgctxt "@title:window"
msgid "New Folder"
msgstr "Создание папки"

#: platformtheme/kdirselectdialog.cpp:129
#, kde-format
msgctxt "@label:textbox"
msgid ""
"Create new folder in:\n"
"%1"
msgstr ""
"Создать новую папку в:\n"
"%1"

#: platformtheme/kdirselectdialog.cpp:160
#, kde-format
msgid "A file or folder named %1 already exists."
msgstr "Файл или папка с именем %1 уже существует."

#: platformtheme/kdirselectdialog.cpp:169
#, kde-format
msgid "You do not have permission to create that folder."
msgstr "У вас нет прав для создания этой папки."

#: platformtheme/kdirselectdialog.cpp:271
#, kde-format
msgctxt "@title:window"
msgid "Select Folder"
msgstr "Выбор папки"

#: platformtheme/kdirselectdialog.cpp:279
#, kde-format
msgctxt "@action:button"
msgid "New Folder..."
msgstr "Создать папку..."

#: platformtheme/kdirselectdialog.cpp:336
#, kde-format
msgctxt "@action:inmenu"
msgid "New Folder..."
msgstr "Создать папку..."

#: platformtheme/kdirselectdialog.cpp:345
#, kde-format
msgctxt "@action:inmenu"
msgid "Move to Trash"
msgstr "Удалить в корзину"

#: platformtheme/kdirselectdialog.cpp:354
#, kde-format
msgctxt "@action:inmenu"
msgid "Delete"
msgstr "Удалить"

#: platformtheme/kdirselectdialog.cpp:365
#, kde-format
msgctxt "@option:check"
msgid "Show Hidden Folders"
msgstr "Показывать скрытые папки"

#: platformtheme/kdirselectdialog.cpp:372
#, kde-format
msgctxt "@action:inmenu"
msgid "Properties"
msgstr "Свойства"

#: platformtheme/kfiletreeview.cpp:185
#, kde-format
msgid "Show Hidden Folders"
msgstr "Показывать скрытые папки"

# BUGME: "Выбор файла для открытия"? --aspotashev
#~ msgid "Opening..."
#~ msgstr "Открытие"

# BUGME: "Выбор файла для сохранения"? --aspotashev
#~ msgid "Saving..."
#~ msgstr "Сохранение"
